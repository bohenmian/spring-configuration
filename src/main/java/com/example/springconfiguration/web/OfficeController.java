package com.example.springconfiguration.web;

import com.example.springconfiguration.model.Office;
import com.example.springconfiguration.respository.OfficeRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class OfficeController {

    private OfficeRepository repository;

    public OfficeController(OfficeRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/office/{officeId}")
    public Office getOffice(@PathVariable Long officeId) {
        return repository.findById(officeId).orElseThrow(RuntimeException::new);
    }

    @PostMapping("api/office")
    public Office save(@RequestBody Office office) {
        Office savedOffice = repository.save(office);
        return savedOffice;
    }
}
