package com.example.springconfiguration.respository;

import com.example.springconfiguration.model.Office;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficeRepository extends JpaRepository<Office, Long> {
}
