package com.example.springconfiguration;

import com.example.springconfiguration.model.Office;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "h2")
public class H2DevTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_h2_database_data_when_config_h2() throws Exception {
        mockMvc.perform(post("/api/office")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsBytes(new Office(7L, "xiang gang"))))
                .andExpect(jsonPath("id").value(7))
                .andExpect(jsonPath("city").value("xiang gang"));
        mockMvc.perform(get("/api/office/7"))
                .andExpect(jsonPath("city").value("xiang gang"));
    }
}
