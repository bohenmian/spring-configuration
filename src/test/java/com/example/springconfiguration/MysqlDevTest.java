package com.example.springconfiguration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class MysqlDevTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_mysql_database_data_when_config_mysql() throws Exception {
        mockMvc.perform(get("/api/office/1"))
                .andExpect(jsonPath("city").value("xian"));
    }
}
